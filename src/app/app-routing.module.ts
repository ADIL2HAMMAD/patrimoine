import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { MuseePatrimoineComponent } from './musee-patrimoine/musee-patrimoine.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MuseeDescComponent } from './musee-desc/musee-desc.component';
import { PatrimoineArchitecturalComponent } from './patrimoine-architectural/patrimoine-architectural.component';

const routes: Routes = [
  {
    path : '' , 
    component : WelcomePageComponent, 
  },
  {
    path : 'musee-patri' , 
    component : MuseePatrimoineComponent, 
  },
  {
    path : 'nav' , 
    component : NavbarComponent, 
  },
  {
    path : 'musee-desc' , 
    component : MuseeDescComponent, 
  },
  {
    path : 'patrimoine-architectural' , 
    component : PatrimoineArchitecturalComponent, 
  }
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { }
