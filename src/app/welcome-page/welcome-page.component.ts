import { Component, OnInit } from '@angular/core';

import { GlobalService } from '../global.service';

@Component({
	selector: 'app-welcome-page',
	templateUrl: './welcome-page.component.html',
	styleUrls:
		[
			'./welcome-page.component.css'
		]
})
export class WelcomePageComponent {
	constructor(private globals: GlobalService) {}

	useLanguage(lang: string) {
		this.globals.applyselectedlang(lang);
	}

	ngOnInit() {}
}
