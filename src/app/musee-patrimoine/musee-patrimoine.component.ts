import { Component, OnInit, Inject } from '@angular/core';
import { GlobalService } from '../global.service';
import { StatsService } from '../stats.service';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
	selector: 'app-musee-patrimoine',
	templateUrl: './musee-patrimoine.component.html',
	styleUrls:
		[
			'./musee-patrimoine.component.css'
		]
})
export class MuseePatrimoineComponent implements OnInit {
	currentlang: string;
	constructor(private globals: GlobalService , @Inject(DOCUMENT) private document: Document) {
		this.currentlang = this.globals.getlang();
	}

	ngOnInit() {
		this.document.body.classList.add('musee-patri-bg');
	}

	ngOnDestroy()  {
		this.document.body.classList.remove('musee-patri-bg');
	}


	setSection(section: string) {
		this.globals.setCurrentSection(section);
	}
}
