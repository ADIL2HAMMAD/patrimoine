import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuseePatrimoineComponent } from './musee-patrimoine.component';

describe('MuseePatrimoineComponent', () => {
  let component: MuseePatrimoineComponent;
  let fixture: ComponentFixture<MuseePatrimoineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuseePatrimoineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuseePatrimoineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
