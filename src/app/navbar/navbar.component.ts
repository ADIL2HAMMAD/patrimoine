import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GlobalService } from '../global.service';

@Component({
	selector: 'app-navbar',
	templateUrl: './navbar.component.html',
	styleUrls:
		[
			'./navbar.component.css'
		]
})
export class NavbarComponent {
	@Output() Currentlang = new EventEmitter<string>();

	language: string = this.globals.getlang();
	constructor(private globals: GlobalService) {}

	useLanguage(lang: string) {
		this.globals.applyselectedlang(lang);
		this.language = this.globals.getlang();
		this.Currentlang.emit(lang);
	}

	initializeGlobalState() {
		this.globals.setCurrentCity("");
		this.globals.setCurrentSection("");
	}

	ngOnInit() {}
}
