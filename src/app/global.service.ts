import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { StatsService } from './stats.service';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  mylang: string = 'fr';
  urlafter: string;
  urlbefore: string;
  test: string;

  private currentCity: string;
  private currentSection: string;

  constructor(private translate: TranslateService, private statsService: StatsService) {}

  setlang(languge: string) {
    this.mylang = languge;
  }

  getlang(): string {
    return this.mylang;
  }

  applyselectedlang(lang: string) {
    this.setlang(lang);
    this.translate.use(this.getlang());

    this.statsService.sendStat({ name: 'SELECT_LANG', value: lang, type: 'text'  });
  }

  geturls(idmage): string[] {
    switch (idmage) {
      case 1: {
        this.urlbefore = 'assets/images/AVANT_APRES/AVANT_APRES_1/avant.jpg';
        this.urlafter = 'assets/images/AVANT_APRES/AVANT_APRES_1/apres.jpg';
        break;
      }
      case 2: {
        this.urlbefore = 'assets/images/AVANT_APRES/AVANT_APRES_2/avant.jpg';
        this.urlafter = 'assets/images/AVANT_APRES/AVANT_APRES_2/apres.jpg';
        break;
      }
      case 3: {
        this.urlbefore = 'assets/images/AVANT_APRES/AVANT_APRES_3/avant.jpg';
        this.urlafter = 'assets/images/AVANT_APRES/AVANT_APRES_3/apres.jpg';
        break;
      }
      case 4: {
        this.urlbefore = 'assets/images/AVANT_APRES/AVANT_APRES_4/avant.jpg';
        this.urlafter = 'assets/images/AVANT_APRES/AVANT_APRES_4/apres.jpg';
        break;
      }
      case 5: {
        this.urlbefore = 'assets/images/AVANT_APRES/AVANT_APRES_5/avant.jpg';
        this.urlafter = 'assets/images/AVANT_APRES/AVANT_APRES_5/apres.jpg';
        break;
      }
      default: {
        break;
      }
    }

    let urls: string[] = [
      this.urlbefore,
      this.urlafter
    ];

    return urls;
  }

  getAgences() {
    let agences = [
      {
        id: '1',
        nom: "Agadir",
        left: '440px',
        top: '385px',
        imgs:
        {
          "1" : "assets/agences/Agence_Agadir/1.jpg",
          "2" : "assets/agences/Agence_Agadir/2.jpg",
          "3" : "assets/agences/Agence_Agadir/3.jpg",
          "4" : "assets/agences/Agence_Agadir/4.jpg",
          "5" : "assets/agences/Agence_Agadir/5.jpg",
        },
        "tmb" : "assets/agences/Agence_Agadir/tmb.jpg",
      },
      {
        id: '2',
        nom: "Al Hociema",
        left: '722px',
        top: '107px',
        imgs:
        {
          "1" : "assets/agences/Agence_Al_Hociema/1.jpg",
          "2" : "assets/agences/Agence_Al_Hociema/2.jpg"
        },
        "tmb" : "assets/agences/Agence_Al_Hociema/tmb.jpg",
      },
      {
        id: '3',
        nom: "Beni Mellal",
        left: '614px',
        top: '280px',
        imgs:
        {
          "1" : "assets/agences/Agence_Béni-Mellal/1.jpg",
          "2" : "assets/agences/Agence_Béni-Mellal/2.jpg",
          "3" : "assets/agences/Agence_Béni-Mellal/3.jpg",
          "4" : "assets/agences/Agence_Béni-Mellal/4.jpg",
          "5" : "assets/agences/Agence_Béni-Mellal/5.jpg"
        },
        "tmb" : "assets/agences/Agence_Béni-Mellal/tmb.jpg",
      },
      {
        id: '4',
        nom: "Casablanca",
        left: '552px',
        top: '212px',
        imgs:
        {
          "1" : "assets/agences/Agence_Casablanca/1.jpg",
          "2" : "assets/agences/Agence_Casablanca/2.jpg",
          "3" : "assets/agences/Agence_Casablanca/3.jpg",
          "4" : "assets/agences/Agence_Casablanca/4.jpg",
          "5" : "assets/agences/Agence_Casablanca/5.jpg",
        },
        "tmb" : "assets/agences/Agence_Casablanca/tmb.jpg",
      },
      {
        id: '5',
        nom: "Salé (ville voisine de Rabat)",
        left: '575px',
        top: '185px',
        imgs:
        {
          "1" : "assets/agences/Site_Dar_As-Sikkah/1.jpg",
          "2" : "assets/agences/Site_Dar_As-Sikkah/2.jpg",
          "3" : "assets/agences/Site_Dar_As-Sikkah/3.jpg"
        },
        "tmb" : "assets/agences/Site_Dar_As-Sikkah/tmb.jpg",
      },
      {
        id: '6',
        nom: "Eljadida",
        left: '500px',
        top: '225px',
        imgs:
        {
          "1" : "assets/agences/Agence_El_Jadida/1.jpg",
          "2" : "assets/agences/Agence_El_Jadida/2.jpg",
          "3" : "assets/agences/Agence_El_Jadida/3.jpg",
          "4" : "assets/agences/Agence_El_Jadida/4.jpg",
          "5" : "assets/agences/Agence_El_Jadida/5.jpg",
        },
        "tmb" : "assets/agences/Agence_El_Jadida/tmb.jpg",
      },
      {
        id: '7',
        nom: "Fes",
        left: '662px',
        top: '150px',
        imgs:
        {
          "1" : "assets/agences/Agence_Fes_Medina/1.jpg",
          "2" : "assets/agences/Agence_Fes_Medina/2.jpg",
          "3" : "assets/agences/Agence_Fes_Medina/3.jpg",
          "4" : "assets/agences/Agence_Fes_Medina/4.jpg",
          "5" : "assets/agences/Agence_Fes_Medina/5.jpg"
        },
        "tmb" : "assets/agences/Agence_Fes_Medina/tmb.jpg",
      },
      {
        id: '8',
        nom: "Fes",
        left: '662px',
        top: '150px',
        imgs:
        {
          "1" : "assets/agences/Nouvelle_Agence_Fes/1.jpg",
          "2" : "assets/agences/Nouvelle_Agence_Fes/2.jpg",
          "3" : "assets/agences/Nouvelle_Agence_Fes/3.jpg",
          "4" : "assets/agences/Nouvelle_Agence_Fes/4.jpg",
          "5" : "assets/agences/Nouvelle_Agence_Fes/5.jpg",
          "6" : "assets/agences/Nouvelle_Agence_Fes/6.jpg",
        },
        "tmb" : "assets/agences/Nouvelle_Agence_Fes/tmb.jpg",
      },
      {
        id: '9',
        nom: "Laayoune",
        left: '278px',
        top: '533px',
        imgs:
        {
          "1" : "assets/agences/Agence_Laayoune/1.jpg",
          "2" : "assets/agences/Agence_Laayoune/2.jpg",
          "3" : "assets/agences/Agence_Laayoune/3.jpg",
          "4" : "assets/agences/Agence_Laayoune/4.jpg",
        },
        "tmb" : "assets/agences/Agence_Laayoune/tmb.jpg",
      },
      {
        id: '10',
        nom: "Larache",
        left: '622px',
        top: '95px',
        imgs:
        {
          "1" : "assets/agences/Agence_Larache/1.jpg",
          "2" : "assets/agences/Agence_Larache/2.jpg"
        },
        "tmb" : "assets/agences/Agence_Larache/tmb.jpg",
      },
      {
        id: '11',
        nom: "Marrakech",
        left: '518px',
        top: '318px',
        imgs:
        {
          "1" : "assets/agences/Agence_Marrakech_Medina/1.jpg",
          "2" : "assets/agences/Agence_Marrakech_Medina/2.jpg",
          "3" : "assets/agences/Agence_Marrakech_Medina/3.jpg",
          "4" : "assets/agences/Agence_Marrakech_Medina/4.jpg",
        },
        "tmb" : "assets/agences/Agence_Marrakech_Medina/tmb.jpg",
      },
      {
        id: '12',
        nom: "Marrakech",
        left: '518px',
        top: '318px',
        imgs:
        {
          "1" : "assets/agences/Nouvelle_Agence_Marrakech/1.jpg",
          "2" : "assets/agences/Nouvelle_Agence_Marrakech/2.jpg",
          "3" : "assets/agences/Nouvelle_Agence_Marrakech/3.jpg",
          "4" : "assets/agences/Nouvelle_Agence_Marrakech/4.jpg",
          "5" : "assets/agences/Nouvelle_Agence_Marrakech/5.jpg",
          "6" : "assets/agences/Nouvelle_Agence_Marrakech/6.jpg",
        },
        "tmb" : "assets/agences/Nouvelle_Agence_Marrakech/tmb.jpg",
      },
      {
        id: '13',
        nom: "Meknes",
        left:' 657px',
        top: '207px',
        imgs:
        {
          "1" : "assets/agences/Agence_Meknes/1.jpg",
          "2" : "assets/agences/Agence_Meknes/2.jpg",
          "3" : "assets/agences/Agence_Meknes/3.jpg",
          "4" : "assets/agences/Agence_Meknes/4.jpg",
          "5" : "assets/agences/Agence_Meknes/5.jpg",
        },
        "tmb" : "assets/agences/Agence_Meknes/tmb.jpg",
      },
      {
        id: '14',
        nom: "Nador",
        left: '781px',
        top: '107px',
        imgs:
        {
          "1" : "assets/agences/Agence_Nador/1.jpg",
          "2" : "assets/agences/Agence_Nador/2.jpg"
        },
        "tmb" : "assets/agences/Agence_Nador/tmb.jpg",
      },
      {
        id: '15',
        nom: "Oujda",
        left: '806px',
        top: '128px',
        imgs:
        {
          "1" : "assets/agences/Agence_Oujda/1.jpg",
          "2" : "assets/agences/Agence_Oujda/2.jpg",
          "3" : "assets/agences/Agence_Oujda/3.jpg",
          "4" : "assets/agences/Agence_Oujda/4.jpg",
          "5" : "assets/agences/Agence_Oujda/5.jpg",
        },
        "tmb" : "assets/agences/Agence_Oujda/tmb.jpg",
      },
      {
        id: '16',
        nom: "Ourzazatte",
        left: '577px',
        top: '359px',
        imgs:
        {
          "1" : "assets/agences/Agence_Ourzazatte/1.jpg",
          "2" : "assets/agences/Agence_Ourzazatte/2.jpg",
          "3" : "assets/agences/Agence_Ourzazatte/3.jpg",
          "4" : "assets/agences/Agence_Ourzazatte/4.jpg",
          "5" : "assets/agences/Agence_Ourzazatte/5.jpg",
        },
        "tmb" : "assets/agences/Agence_Ourzazatte/tmb.jpg",
      },
      {
        id: '17',
        nom: "Rabat",
        left: '575px',
        top: '183px',
        imgs:
        {
          "1" : "assets/agences/Rabat_Site_Nakhil/1.jpg",
          "2" : "assets/agences/Rabat_Site_Nakhil/2.jpg",
          "3" : "assets/agences/Rabat_Site_Nakhil/3.jpg",
          "4" : "assets/agences/Rabat_Site_Nakhil/4.jpg",
        },
        "tmb" : "assets/agences/Rabat_Site_Nakhil/tmb.jpg",
      },
      {
        id: '18',
        nom: "Rabat",
        left: '575px',
        top: '183px',
        imgs:
        {
          "1" : "assets/agences/Site_Rabat_Ville/1.jpg",
          "2" : "assets/agences/Site_Rabat_Ville/2.jpg",
          "3" : "assets/agences/Site_Rabat_Ville/3.jpg",
          "4" : "assets/agences/Site_Rabat_Ville/4.jpg",
          "5" : "assets/agences/Site_Rabat_Ville/5.jpg",
          "6" : "assets/agences/Site_Rabat_Ville/6.jpg",
          "7" : "assets/agences/Site_Rabat_Ville/7.jpg",
        },
        "tmb" : "assets/agences/Site_Rabat_Ville/tmb.jpg",
      },
      {
        id: '19',
        nom: "Taza",
        left: '761px',
        top: '140px',
        imgs:
        {
          "1" : "assets/agences/Agence_Taza/1.jpg",
          "2" : "assets/agences/Agence_Taza/2.jpg",
          "3" : "assets/agences/Agence_Taza/3.jpg",
          "4" : "assets/agences/Agence_Taza/4.jpg",
          "5" : "assets/agences/Agence_Taza/5.jpg"
        },
        "tmb" : "assets/agences/Agence_Taza/tmb.jpg",
      },
      {
        id: '20',
        nom: "Tétouan",
        left: '657px',
        top: '77px',
        imgs:
        {
          "1" : "assets/agences/Agence_Tétouan/1.jpg",
          "2" : "assets/agences/Agence_Tétouan/2.jpg",
          "3" : "assets/agences/Agence_Tétouan/3.jpg"
        },
        "tmb" : "assets/agences/Agence_Tétouan/tmb.jpg",
      },
      {
        id: '21',
        nom: "kénitra",
        left: '598px',
        top: '144px',
        imgs:
        {
          "1" : "assets/agences/Agence_Kénitra/1.jpg",
          "2" : "assets/agences/Agence_Kénitra/2.jpg",
          "3" : "assets/agences/Agence_Kénitra/3.jpg",
          "4" : "assets/agences/Agence_Kénitra/4.jpg",
          "5" : "assets/agences/Agence_Kénitra/5.jpg"
        },
        "tmb" : "assets/agences/Agence_Kénitra/tmb.jpg",
      },
      {
        id: '22',
        nom: "Safi",
        left: '460px',
        top: '270px',
        imgs:
        {
          "1" : "assets/agences/Agence_Safi/1.jpg",
          "2" : "assets/agences/Agence_Safi/2.jpg",
          "3" : "assets/agences/Agence_Safi/3.jpg",
          "4" : "assets/agences/Agence_Safi/4.jpg",
        },
        "tmb" : "assets/agences/Agence_Safi/tmb.jpg",
      },
      {
        id: '23',
        nom: "Settat",
        left: '568px',
        top: '247px',
        imgs:
        {
          "1" : "assets/agences/Agence_Settat/1.jpg",
          "2" : "assets/agences/Agence_Settat/2.jpg",
          "3" : "assets/agences/Agence_Settat/3.jpg",
          "4" : "assets/agences/Agence_Settat/4.jpg",
        },
        "tmb" : "assets/agences/Agence_Settat/tmb.jpg",
      },
      {
        id: '24',
        nom: "Tanger",
        left: '628px',
        top: '75px',
        imgs:
        {
          "1" : "assets/agences/Agence_Tanger/1.jpg",
          "2" : "assets/agences/Agence_Tanger/2.jpg",
          "3" : "assets/agences/Agence_Tanger/3.jpg",
          "4" : "assets/agences/Agence_Tanger/4.jpg",
          "5" : "assets/agences/Agence_Tanger/5.jpg",
        },
        "tmb" : "assets/agences/Agence_Tanger/tmb.jpg",
      }
    ];
    return agences;
  }

  setCurrentCity(city: string) {
    // Report the unselection of the previous city
    if (this.currentCity) {
      this.statsService.sendStat({
        name: 'UNSELECT_CITY',
        value: JSON.stringify({
          value: this.currentCity,
          lang: this.getlang()
        }),
        type: 'json'
      });
    }

    this.currentCity = city;

    if (city) {
      // Report the selection of the current city
      this.statsService.sendStat({
        name: 'SELECT_CITY',
        value: JSON.stringify({
          value: this.currentCity,
          lang: this.getlang()
        }),
        type: 'json'
      });
    }
  }

  setCurrentSection(section: string) {
    // Report the unselection of the previous city
    if (this.currentSection) {
      this.statsService.sendStat({
        name: 'UNSELECT_SECTION',
        value: this.currentSection,
        type: 'text'
      });
    }

    this.currentSection = section;

    if (section) {
      // Report the selection of the current city
      this.statsService.sendStat({
        name: 'SELECT_SECTION',
        value: this.currentSection,
        type: 'text'
      });
    }
  }
}
