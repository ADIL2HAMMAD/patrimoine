import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatrimoineArchitecturalComponent } from './patrimoine-architectural.component';

describe('PatrimoineArchitecturalComponent', () => {
  let component: PatrimoineArchitecturalComponent;
  let fixture: ComponentFixture<PatrimoineArchitecturalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatrimoineArchitecturalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatrimoineArchitecturalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
