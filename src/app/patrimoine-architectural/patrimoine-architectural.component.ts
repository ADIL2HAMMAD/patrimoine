import { Component, OnInit, Inject } from '@angular/core';
import { GlobalService } from '../global.service';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'app-patrimoine-architectural',
  templateUrl: './patrimoine-architectural.component.html',
  styleUrls:
    [
      './patrimoine-architectural.component.css'
    ]
})
export class PatrimoineArchitecturalComponent implements OnInit {
  showNavigationArrows = false;
  showNavigationIndicators = true;
  constructor(private globals: GlobalService, config: NgbCarouselConfig ,  @Inject(DOCUMENT) private document: Document ) {
    config.showNavigationArrows = false;
    config.showNavigationIndicators = true;
  }

  isSlected: boolean = null; // for Model dispaly or not
  isClicked: boolean = null; // for Marker dispaly or not
  leftValue: string = '0px';
  topValue: string = '10px';
  agence: string = 'Ville';
  val: number;
  agences: {};
  imgs = new Array();
  size = 0;

  // imgs: any[] = [];

  currentlang: string = this.globals.getlang();
  getCurrentlang(lang: string) {
    this.currentlang = lang;
  }

  imageclicked(val: number) {
    this.imgs = [];
    this.isSlected = true;
    this.isClicked = true;
    let index = val - 1;
    this.topValue = this.agences[index].top;
    this.leftValue = this.agences[index].left;
    this.agence = this.agences[index].nom;
    this.val = val;
    //this.imgs.push(this.agences[index].imgs);
    //console.log(Object.entries(this.agences[index].imgs));
    //this.size = Object.keys(this.imgs[0]).length;
    this.imgs = Object.entries(this.agences[index].imgs);

    this.globals.setCurrentCity(this.agence);
  }

  setMyStyles() {
    let styles = {
      visibility: this.isSlected ? 'visible' : 'hidden',
      left: this.leftValue,
      top: this.topValue
    };
    return styles;
  }

  close(val?: any) {
    this.isClicked = false;
    this.isSlected = false;
    this.globals.setCurrentCity("");
  }

  toggleclass() {}

  images = [
    'assets/images/img1.jpg',
    'assets/images/img2.jpg',
    'assets/images/img3.jpg'
  ];
	ngOnDestroy()  {
		this.document.body.classList.remove('musee-archi-bg');
	}

  ngOnInit() {
		this.document.body.classList.add('musee-archi-bg');

    this.agences = this.globals.getAgences();
    console.log(this.agences);
  }
}
