import { Component, OnInit } from '@angular/core';

import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
	selector: 'app-musee-desc-img1',
	templateUrl: './musee-desc-img1.component.html',
	styleUrls:
		[
			'./musee-desc-img1.component.css'
		]
})
export class MuseeDescImg1Component implements OnInit {
	constructor(private SmartModelService: NgxSmartModalService) {}

	urlafter: string;
	urlbefore: string;

	data() {
		this.urlbefore = this.SmartModelService.getModalData('popupOne')[0];
		this.urlafter = this.SmartModelService.getModalData('popupOne')[1];
	}

	close() {
		((document.querySelector('.range') as any) || {}).value = '50';
	}

	affectdata() {
		//this.urlbefore = this.SmartModelService.getModalData('popupOne')[0];
		//this.urlafter = this.SmartModelService.getModalData('popupOne')[1];
	}
	ngOnInit() {}
}
