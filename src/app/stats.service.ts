import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StatsService {
  private socket;
  private sessionStarted = false;

  constructor() {
    const basePath = document.getElementsByTagName('base')[0].getAttribute('href');
    const socketio = (window as any).io;

    if (socketio) {
      this.socket = socketio(location.origin, { path: `${basePath}socket.io` });

      this.socket.on('sessionCreated', sessionId => {
        this.socket.sessionId = sessionId;
        this.sessionStarted = true;
      });

      let intervalId;

      this.socket.on('connect', () => {
        if (intervalId) {
          window.clearInterval(intervalId);
        }

        intervalId = setInterval(() => this.socket.emit('log'), 250);
      });

      this.socket.on('reconnect', () => {
        if (intervalId) {
          window.clearInterval(intervalId);
        }

        intervalId = setInterval(() => this.socket.emit('log'), 250);
      });
    }

    // End the session of the user
    window.onbeforeunload = () => {
      this.endSession();
    };
  }

  iSessionStarted(): boolean {
    return this.sessionStarted;
  }

  startSession() {
    if (!this.socket) {
      console.log('startSession');
      return;
    }

    this.socket.emit('startSession');
  }

  endSession() {
    if (!this.socket) {
      console.log('endSession');
      return;
    }

    this.socket.emit('endSession', this.socket.sessionId);
    this.socket.sessionId = undefined;
    this.sessionStarted = false;
  }

  sendStat(stat) {
    if (!this.socket) {
      console.log('stats', stat);
      return;
    }

    stat.sessionId = this.socket.sessionId;

    this.socket.emit('stats', stat);
  }
}
