import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuseeDescComponent } from './musee-desc.component';

describe('MuseeDescComponent', () => {
  let component: MuseeDescComponent;
  let fixture: ComponentFixture<MuseeDescComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuseeDescComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuseeDescComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
