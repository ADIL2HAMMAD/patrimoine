import { DOCUMENT } from '@angular/platform-browser';
import { Component, OnInit, Inject } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { GlobalService } from '../global.service';

@Component({
	selector: 'app-musee-desc',
	templateUrl: './musee-desc.component.html',
	styleUrls:
	[
		'./musee-desc.component.css'
	]
})
export class MuseeDescComponent {
	constructor(
		private SmartModelService: NgxSmartModalService,
		private globals: GlobalService ,
		@Inject(DOCUMENT) private document: Document) {}

		urlafter: string;
		urlbefore: string;
		urls: string[];

		currentlang: string = this.globals.getlang();
		getCurrentlang(lang: string) {
			this.currentlang = lang;
		}

		openModel(model: string, idmage: number) {
			this.urls = this.globals.geturls(idmage);
			this.SmartModelService.getModal(model).open();
			this.SmartModelService.resetModalData('popupOne');
			this.SmartModelService.setModalData(this.urls, 'popupOne');
		}

		ngOnDestroy()  {
			this.document.body.classList.remove('musee-patri-bg');
		}

		ngOnInit() {
			this.document.body.classList.add('musee-patri-bg');
		}
	}
