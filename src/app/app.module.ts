import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxSmartModalModule, NgxSmartModalService } from 'ngx-smart-modal';

// import ngx-translate and the http loader
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { MuseePatrimoineComponent } from './musee-patrimoine/musee-patrimoine.component';
import { MuseeDescComponent } from './musee-desc/musee-desc.component';
import { PatrimoineArchitecturalComponent } from './patrimoine-architectural/patrimoine-architectural.component';
import { MuseeDescImg1Component } from './models-poppup/musee-desc-img1/musee-desc-img1.component';

import { FormsModule } from '@angular/forms';
import { GlobalService } from './global.service';
import { FooterComponent } from './footer/footer.component';

@NgModule({
	declarations:
		[
			AppComponent,
			NavbarComponent,
			WelcomePageComponent,
			MuseePatrimoineComponent,
			MuseeDescComponent,
			PatrimoineArchitecturalComponent,
			MuseeDescImg1Component,
			FooterComponent
		],
	imports:
		[
			NgxSmartModalModule.forRoot(),
			BrowserModule,
			BrowserAnimationsModule,
			AppRoutingModule,
			FormsModule,
			AngularFontAwesomeModule,
			NgbModule,

			// configure the imports
			HttpClientModule,
			TranslateModule.forRoot({
				loader:
					{
						provide: TranslateLoader,
						useFactory: HttpLoaderFactory,
						deps:
							[
								HttpClient
							]
					}
			})
		],
	providers:
		[
			NgxSmartModalService,
			GlobalService
		],
	bootstrap:
		[
			AppComponent
		]
})
export class AppModule {}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http, document.baseURI + "assets/i18n/");
}
