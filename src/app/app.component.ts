import { Component, HostListener } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

import { GlobalService } from './global.service';
import { StatsService } from "./stats.service";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls:
		[
			'./app.component.css'
		]
})
export class AppComponent {
	constructor(private translate: TranslateService, private globals: GlobalService, private router: Router, private statsService: StatsService) {
		translate.setDefaultLang(this.globals.getlang());

    // Start the session
    this.statsService.startSession();
	}

	d: any = new Date();
	diff: any = 0;
	maxTimeInMinute: number = 5;
	showSaveScreen = false;
	index = 0;

	@HostListener('document:touchstart', [
		'$event'
	])
	onTouchStart($event: TouchEvent): void {
		this.d = new Date();
		this.showSaveScreen = false;
		console.log('Save Screen Disabled');

		// Start a new session in case there is no active session
		if (!this.statsService.iSessionStarted()) {
			this.statsService.startSession();
		}
	}

	useLanguage() {
		this.globals.applyselectedlang(this.globals.getlang());
	}

	ngOnInit() {
		// display a Save Screen
		setInterval(() => {
			let nd: any = new Date();
			this.diff = Math.abs(nd - this.d);

			if (Math.floor(this.diff / 1000) === this.maxTimeInMinute * 60) {
				console.log('Save Screen Active : ' + this.diff);

				this.router.navigate([
					'/'
				]);

				this.showSaveScreen = true;

				// End the current session
				this.globals.setCurrentCity("");
				this.globals.setCurrentSection("");
				this.statsService.endSession();
			}
		}, 1000);

		// animation for Touch here
		let title1 = document.querySelector('.title1');
		let title2 = document.querySelector('.title2');
		let title3 = document.querySelector('.title3');

		setInterval(() => {
			if (this.showSaveScreen) {
				if (title1.classList.contains('active')) {
					title1.classList.remove('active');
					setTimeout(() => title2.classList.add('active'));
				}
				else if (title2.classList.contains('active')) {
					title2.classList.remove('active');
					setTimeout(() => title3.classList.add('active'));
				}
				else {
					title3.classList.remove('active');
					setTimeout(() => title1.classList.add('active'));
				}
			}
		}, 4000);
	}
}
